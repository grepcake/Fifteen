package com.example.artyom.fifteen


import android.content.Context
import android.util.Log
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import com.example.artyom.fifteen.MainApplication.Companion.BLANK
import com.example.artyom.fifteen.MainApplication.Companion.X_SIZE
import com.example.artyom.fifteen.MainApplication.Companion.Y_SIZE
import com.example.artyom.fifteen.MainApplication.Companion.blankX
import com.example.artyom.fifteen.MainApplication.Companion.blankY
import com.example.artyom.fifteen.MainApplication.Companion.field
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.Math.abs

internal class TouchListener(private val mainActivity: MainActivity, context: Context) : View.OnTouchListener {
    var detector: GestureDetector

    init {
        detector = GestureDetector(context, GestureListener())
    }

    override fun onTouch(view: View, motionEvent: MotionEvent) = detector.onTouchEvent(motionEvent)

    private inner class GestureListener : GestureDetector.SimpleOnGestureListener() {
        override fun onFling(e1: MotionEvent, e2: MotionEvent, velocityX: Float, velocityY: Float): Boolean {
            val diffX = (e2.x - e1.x).toDouble()
            val diffY = (e2.y - e1.y).toDouble()

            if (abs(diffX) > abs(diffY)) {
                if (diffX > 0) {
                    onSwipeRight()
                } else {
                    onSwipeLeft()
                }
            } else {
                if (diffY > 0) {
                    onSwipeDown()
                } else {
                    onSwipeUp()
                }
            }

            mainActivity.mainGrid.invalidateViews()

            mainActivity.checkIfHasWon()

            return false
        }

        private fun onSwipeDown() {
            Log.d(mainActivity.SWIPE_TAG, "down")

            if (blankY > 0) {
                field[blankY][blankX] = field[blankY - 1][blankX]
                field[blankY - 1][blankX] = BLANK
                blankY = blankY - 1
            }
        }

        private fun onSwipeUp() {
            Log.d(mainActivity.SWIPE_TAG, "up")

            if (blankY < Y_SIZE - 1) {
                field[blankY][blankX] = field[blankY + 1][blankX]
                field[blankY + 1][blankX] = BLANK
                blankY = blankY + 1
            }
        }

        private fun onSwipeRight() {
            Log.d(mainActivity.SWIPE_TAG, "right")

            if (blankX > 0) {
                field[blankY][blankX] = field[blankY][blankX - 1]
                field[blankY][blankX - 1] = BLANK
                blankX = blankX - 1
            }
        }

        private fun onSwipeLeft() {
            Log.d(mainActivity.SWIPE_TAG, "left")

            if (blankX < X_SIZE - 1) {
                field[blankY][blankX] = field[blankY][blankX + 1]
                field[blankY][blankX + 1] = BLANK
                blankX = blankX + 1
            }
        }
    }
}
