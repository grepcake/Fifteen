package com.example.artyom.fifteen

import android.app.Application
import android.util.Log
import java.util.*

class MainApplication : Application() {

    internal val APPLICATION_TAG = "Application"

    override fun onCreate() {
        super.onCreate()
        Log.e(APPLICATION_TAG, "onCreate")

        generateField()
    }

    internal fun generateField() {
        val numbers = generateCorrectPermutation()

        for (index in 0..SIZE - 1) {
            field[index / X_SIZE][index % X_SIZE] = numbers[index]
        }

        val blankPosition = numbers.indexOf(BLANK)
        blankX = blankPosition % X_SIZE
        blankY = blankPosition / X_SIZE
    }

    private fun generateCorrectPermutation(): ArrayList<Int> {
        val numbers = ArrayList<Int>(SIZE)
        for (i in 1..SIZE - 1) {
            numbers.add(i)
        }
        numbers.add(0)
        Collections.shuffle(numbers)

        if (isSolvable(numbers)) {
            return numbers
        }

        return generateCorrectPermutation()
    }

    private fun isSolvable(permutation: ArrayList<Int>): Boolean {
        // maths https://en.wikipedia.org/wiki/15_puzzle#Solvability
        var n = 0
        for (num in 1..SIZE - 1) {
            val curPos = permutation.indexOf(num)
            var numOfLess = 0
            for (i in curPos + 1..SIZE - 1) {
                numOfLess += if (permutation[i] < num && permutation[i] != BLANK) 1 else 0
            }
            n += numOfLess
        }
        n += permutation.indexOf(BLANK) / X_SIZE
        return n % 2 != 0
    }

    companion object {

        internal val X_SIZE = 4
        internal val Y_SIZE = 4
        internal val SIZE = X_SIZE * Y_SIZE
        internal var field = Array(Y_SIZE) { IntArray(X_SIZE) }

        internal val BLANK = 0
        internal var blankX = 0
        internal var blankY = 0
    }

}
